#include "stdafx.h"
#include <iostream>
#include <cstdlib>
using namespace std;
void place(int lokasi);
int dice(int x);
void track(int x);
int main()
{
	int cache, player1 = 1, player2 = 0, loc1 = 0, loc2 = 0, length;
	cout << "Track length: "; cin >> length;
	while (true) {
		salah:
		system("cls");
		track(length);
		place(loc1);
		place(loc2);
		track(length);
		if (loc1 >= length) {
			cout << "Player 1 win!\n\a";
			system("pause");
			break;
		}
		else if (loc2 >= length) {
			cout << "Player 2 win!\n\a";
			system("pause");
			break;
		}
		if (player1 == 1) {
			cout << "Place your lucky numbers, Player 1!: "; cin >> cache;
			if (cache < 1) {
				cout << "Try again\n";
				system("pause");
				goto salah;
			}
			cache = dice(cache);
			cout << "Anda mendapatkan " << cache << " langkah!\n";
			loc1 += cache;
			player1 = 0; player2 = 1;
			system("pause");
		} else if (player2 == 1) {
			cout << "Place your lucky numbers, Player 2!: "; cin >> cache;
			if (cache < 1) {
				cout << "Try again\n";
				system("pause");
				goto salah;
			}
			cache = dice(cache);
			cout << "Anda mendapatkan " << cache << " langkah!\n";
			loc2 += cache;
			player1 = 1; player2 = 2;
			system("pause");
		}
	}
    return 0;
}

void track(int x) {
	for (int y = 1; y <= x; y++) {
		cout << "=";
	}
	cout << endl;
}

int dice(int x) {
	int z = 0, y;
	for (y = 0; y < x; y++) {
		z += rand() % 15 + 1;
	}
	x = z / y;
	return x;
}

void place(int lokasibelakang) {
	for (int x = 1; x <= lokasibelakang; x++) {
		cout << "-";
	}
	cout << "X\n";
}